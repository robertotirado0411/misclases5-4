/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author Edgar Guerrero
 */
public class Terreno {
    private float ancho;
    private float largo;
    
    
    
    
    public Terreno() {
        //omision
        this.ancho=0;
        this.largo=0;   
    }
    public Terreno(float ancho, float largo){
        //argumento
        this.ancho=ancho;
        this.largo=largo;
        
    }
    public Terreno( Terreno x){
        //copia
        this.ancho=x.ancho;
        this.largo=x.largo;
    }
    //set y get

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    // metodos de comportamiento
    public float calcularPerimetro(){
        float perimetro=0.0f;
        perimetro=this.ancho*2+this.largo*2;
        return perimetro;
        
    }
    public float calcularArea(){
        float area=0.0f;
        area=this.ancho*this.largo;
        return area;
    }
    public void imprimirTerrerno(){
        System.out.println("Ancho = "+this.ancho);
        System.out.println ("Largo = "+this.largo);
        System.out.println("El perimetro es "+this.calcularPerimetro());
        System.out.println("El area es"+ this.calcularArea());
        
    }
}
