/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author Edgar Guerrero
 */
public class Factura {
    
    private int numFactura;
    private String rfc;
    private String nombreClinete;
    private String domicilioFiscal;
    private String descripcion;
    private String fechaVenta;
    private float totalVenta;
    
    //constructores

    public Factura() {
        //constructor por omision
        this.numFactura =0;
        this.rfc ="";
        this.nombreClinete ="";
        this.domicilioFiscal ="";
        this.descripcion ="";
        this.fechaVenta ="";
        this.totalVenta =0.0f;
        
    }
        //constructor por argumentos
    public Factura(int numFactura, String rfc, String nombreClinete, String domicilioFiscal, String descripcion, String fechaVenta, float totalVenta) {
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombreClinete = nombreClinete;
        this.domicilioFiscal = domicilioFiscal;
        this.descripcion = descripcion;
        this.fechaVenta = fechaVenta;
        this.totalVenta = totalVenta;
    }
    
        //constructor por copia
    public Factura( Factura otro ){
        this.numFactura = otro.numFactura;
        this.rfc = otro.rfc;
        this.nombreClinete = otro.nombreClinete;
        this.domicilioFiscal = otro.domicilioFiscal;
        this.descripcion = otro.descripcion;
        this.fechaVenta = otro.fechaVenta;
        this.totalVenta = otro.totalVenta;
    }
        //metodos get/set
    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreClinete() {
        return nombreClinete;
    }

    public void setNombreClinete(String nombreClinete) {
        this.nombreClinete = nombreClinete;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
    
        //metodos de comportamiento
    public float calcularImpuesto(){
        float impuesto =0.0f;
        impuesto = this.totalVenta * .16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float total = 0.0f;
        total = this.totalVenta + this.calcularImpuesto();
        return total;
    }    
    
    
    public void imprimirFactura(){
        System.out.println("Numero de factura :" + this.numFactura);
        System.out.println("RFC :" + this.rfc);
        System.out.println("Nombre del cliente :" + this.nombreClinete);
        System.out.println("Domicilio Fiscal :" + this.domicilioFiscal);
        System.out.println("Descripcion :" + this.descripcion);
        System.out.println("Fecha de venta :" + this.fechaVenta);
        System.out.println("Total venta :" + this.totalVenta);
        System.out.println("Impuesto :" + this.calcularImpuesto());
        System.out.println("Total a pagar :" + this.calcularTotalPagar());
        
        
    }  
    
    
}
