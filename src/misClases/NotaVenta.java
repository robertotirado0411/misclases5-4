/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author Edgar Guerrero
 */
public class NotaVenta {
    private int numero;
    private String nombreCliente;
    private Fecha fechaVenta;
    private String concepto=null;
    private float total;
    private int tipo;// 1 CREDITO 2 CONTADO 

    public NotaVenta() {
        this.numero=0;
        this.nombreCliente="";
        this.concepto="";
        this.total=0.0f;
        this.tipo=0;
        this.fechaVenta= new Fecha();
        
        
    }

    public NotaVenta(int numero, String nombreCliente, Fecha fechaVenta, float total, int tipo) {
        this.numero = numero;
        this.nombreCliente = nombreCliente;
        this.fechaVenta = fechaVenta;
        this.total = total;
        this.tipo = tipo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Fecha getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Fecha fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    //CALCULAR IMPUESTOS 16%
    public float calcularImpuesto(){
        float impuesto =0.0f;
        impuesto = this.total * .16f;
        return impuesto;
    }    
    
    //TOTAL PAGAR INCLUYENDO IMPUESTOS
    public float calcularTotalPagar(){
        float total = 0.0f;
        total = this.calcularImpuesto() + this.total;
        return total;
    }      
    
    
    
    
    
    
}
